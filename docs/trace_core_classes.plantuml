'
' Copyright (c) 2021 Contributors to the Eclipse Foundation
'
' This program and the accompanying materials are made
' available under the terms of the Eclipse Public License 2.0
' which is available at https://www.eclipse.org/legal/epl-2.0/
'
' SPDX-License-Identifier: EPL-2.0
'

@startuml
interface IAttributeAware {
  String getAttributeValue(String key)
  void setAttributeValue(String key, String value)
}

interface IDependency {
  IEvent getSrc()
  IEvent getDst()
}

interface IEvent {
  Number getTimeStamp()
}

interface IClaimEvent {
  IClaim getClaim()
  ClaimEventType getType()
}

interface IResource {
  Number getCapacity()
  boolean usesOffset()
}

interface IClaim {
  Number getStartTime()
  Number getEndTime()
  IResource getResource()
  Number getAmount()
  Number getOffset()
  IClaimEvent getStartEvent()
  IClaimEvent  getEndEvent()
}

interface IPsop {
  List<IPsopFragment> getFragments()
}

interface IPsopFragment {
  Number getA()
  Number getB()
  Number getC()
  IInterval dom()
  Shape getShape()
}

interface ITrace {
  TimeUnit getTimeUnit()
  Number getTimeOffset()
  List<IEvent> getEvents()
  List<IResource> getResources()
  List<IClaim> getClaims()
  List<IDependency> getDependencies()
  List<IPsop> getSignals()
}

interface IInterval {
  Number lb()
  Number ub() 
  boolean isOpenLb()
  boolean isOpen()
}

enum Shape {
  CONSTANT
  INCREASING
  DECREASING
  PARABOLA_CUP
  PARABOLA_CAP
}

IAttributeAware <|-- IDependency

IAttributeAware <|-- IEvent
IDependency *--> IEvent

IEvent <|-- IClaimEvent

IAttributeAware <|-- IResource

IAttributeAware <|-- IClaim
IClaim *--> IResource
IClaim *-- IClaimEvent

IAttributeAware <|-- IPsop
IPsop *--> IPsopFragment

IAttributeAware <|-- ITrace
ITrace *--> IEvent
ITrace *--> IClaim
ITrace *--> IResource
ITrace *--> IDependency
ITrace *--> IPsop
@enduml