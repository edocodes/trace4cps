////
  // Copyright (c) 2021 Contributors to the Eclipse Foundation
  //
  // This program and the accompanying materials are made
  // available under the terms of the Eclipse Public License 2.0
  // which is available at https://www.eclipse.org/legal/epl-2.0/
  //
  // SPDX-License-Identifier: EPL-2.0
////

include::_initCommon.adoc[]

[[release-notes]]
==  pass:normal[{trace} release notes]

The release notes of {trace} are listed below, in reverse chronological order.

[[release-notes-v0.2]]
=== Version 0.2

TBD

[[release-notes-v0.1]]
=== Version 0.1 (2022-06-17)

The first release of the {trace} project.
This version is a port of https://esi.nl/research/output/tools/trace[ESI TRACE v2.0] that is prepared for its release to the Eclipse foundation.
