/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.tl;

import java.util.Objects;

import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.tl.etl.Formula;

public class FormulaTuple {
    Formula etl;

    MtlFormula mtl;

    String check;

    public FormulaTuple(Formula etl, MtlFormula mtl, String check) {
        super();
        this.etl = etl;
        this.mtl = mtl;
        this.check = check;
    }

    public Formula getEtl() {
        return etl;
    }

    public MtlFormula getMtl() {
        return mtl;
    }

    public String getCheck() {
        return check;
    }

    @Override
    public int hashCode() {
        return Objects.hash(etl, mtl, check);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FormulaTuple other = (FormulaTuple)obj;
        return Objects.equals(etl, other.etl) && Objects.equals(mtl, other.mtl) && Objects.equals(check, other.check);
    }

    @Override
    public String toString() {
        return "(" + this.etl.toString() + ", " + this.mtl.toString() + ", " + this.check.toString() + ")";
    }
}
