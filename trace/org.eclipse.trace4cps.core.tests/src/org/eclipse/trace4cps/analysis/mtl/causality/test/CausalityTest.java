/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl.causality.test;

import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.AND;
import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.F;
import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.G;
import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.IMPLY;
import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.NOT;
import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.OR;
import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.TRUE;
import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.U;
import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.X;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.trace4cps.analysis.mtl.DefaultAtomicProposition;
import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.analysis.mtl.VerificationSemantics;
import org.eclipse.trace4cps.analysis.mtl.causality.LtlCausalityChecker;
import org.eclipse.trace4cps.analysis.mtl.causality.LtlNnf;
import org.eclipse.trace4cps.analysis.mtl.causality.MtlNnf;
import org.eclipse.trace4cps.analysis.mtl.causality.RecursiveMemoizationCausalityChecker;
import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.impl.Event;
import org.junit.jupiter.api.Test;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;

public class CausalityTest {
    @Test
    public void causalityetf() {
        List<IEvent> trace = Lists.newArrayList( //
                new Event(0, "e", "1", "a", "1"), //
                new Event(1, "e", "1", "a", "1"), //
                new Event(2, "e", "1"), //
                new Event(3, "e", "2") //
        );

        MtlFormula e1 = new DefaultAtomicProposition("e", "1");
        MtlFormula e2 = new DefaultAtomicProposition("e", "2");
        MtlFormula phi1 = G(NOT(e2));
        MtlFormula phi2 = NOT(F(e2));
        MtlFormula phi3 = NOT(U(TRUE(), e2));
        MtlFormula phi4 = NOT(U(NOT(NOT(TRUE())), e2));

        for (MtlFormula phi: ImmutableList.of(phi1, phi2, phi3, phi4)) {
            compareLtl_G_R_newU_Mtl(trace, phi, "[(3, e=2)]");
        }

        MtlFormula phi5 = G(OR(e1, e2));
        compareLtl_G_R_newU_Mtl(trace, phi5, "[]");
    }

    @Test
    public void fig1() {
        List<IEvent> trace = Lists.newArrayList(//
                new Event(0, "start", "false", "end", "false", "status_valid", "false", "ready", "false"), //
                new Event(1, "start", "true", "end", "false", "status_valid", "false", "ready", "false"), //
                new Event(2, "start", "false", "end", "false", "status_valid", "false", "ready", "false"), //
                new Event(3, "start", "false", "end", "true", "status_valid", "false", "ready", "false"), //
                new Event(4, "start", "true", "end", "false", "status_valid", "true", "ready", "true"), //
                new Event(5, "start", "false", "end", "false", "status_valid", "false", "ready", "false"), //
                new Event(6, "start", "false", "end", "true", "status_valid", "false", "ready", "false"), //
                new Event(7, "start", "false", "end", "false", "status_valid", "false", "ready", "false"), //
                new Event(8, "start", "false", "end", "false", "status_valid", "false", "ready", "false"), //
                new Event(9, "start", "true", "end", "false", "status_valid", "true", "ready", "false"), //
                new Event(10, "start", "false", "end", "false", "status_valid", "true", "ready", "true"), //
                new Event(11, "start", "false", "end", "false", "status_valid", "false", "ready", "false") //
        );
        MtlFormula start = new DefaultAtomicProposition("start", "true");
        MtlFormula status_valid = new DefaultAtomicProposition("status_valid", "true");
        MtlFormula end = new DefaultAtomicProposition("end", "true");
        MtlFormula ready = new DefaultAtomicProposition("ready", "true");
        MtlFormula phi = //
                G( //
                        IMPLY( //
                                AND(AND(NOT(start), NOT(status_valid)), end), //
                                X( //
                                        U( //
                                                NOT(start), //
                                                AND(status_valid, ready)))));

        compareLtl_G_R_newU_Mtl(trace, phi,
                "[(6, end=true), (6, start=true), (6, status_valid=true), (7, ready=true), (7, status_valid=true), (8, ready=true), (8, status_valid=true), (9, ready=true), (9, start=true)]");
    }

    @Test
    public void fig10() {
        List<IEvent> trace = Lists.newArrayList(//
                new Event(0, "large_packet_mode", "large_packet_mode", "status_ok", "status_ok"), //
                new Event(1, "large_packet_mode", "large_packet_mode", "status_ok", "status_ok"), //
                new Event(2, "status_valid", "status_valid", "large_packet_mode", "large_packet_mode", "status_ok",
                        "status_ok"), //
                new Event(3, "status_valid", "status_valid", "large_packet_mode", "large_packet_mode",
                        "long_frame_received", "long_frame_received", "transfer_stopped", "transfer_stopped"), //
                new Event(4, "long_frame_error", "long_frame_error"), //
                new Event(5, "status_ok", "status_ok"), //
                new Event(6, "status_valid", "status_valid", "status_ok", "status_ok"), //
                new Event(7, "status_valid", "status_valid", "status_ok", "status_ok"), //
                new Event(8, "large_packet_mode", "large_packet_mode", "long_frame_received", "long_frame_received",
                        "status_ok", "status_ok"), //
                new Event(9, "large_packet_mode", "large_packet_mode", "long_frame_received", "long_frame_received",
                        "status_ok", "status_ok"), //
                new Event(10, "large_packet_mode", "large_packet_mode", "status_ok", "status_ok"), //
                new Event(11, "large_packet_mode", "large_packet_mode", "status_ok", "status_ok"), //
                new Event(12, "status_valid", "status_valid", "long_frame_received", "long_frame_received", "status_ok",
                        "status_ok"), //
                new Event(13, "status_valid", "status_valid", "long_frame_received", "long_frame_received"), //
                new Event(14, "long_frame_error", "long_frame_error"), //
                new Event(15, "status_valid", "status_valid", "long_frame_received", "long_frame_received", "status_ok",
                        "status_ok"), //
                new Event(16, "status_valid", "status_valid", "large_packet_mode", "large_packet_mode",
                        "long_frame_received", "long_frame_received"), //
                new Event(17, "large_packet_mode", "large_packet_mode", "long_frame_error", "long_frame_error"), //
                new Event(18, "large_packet_mode", "large_packet_mode", "status_ok", "status_ok"), //
                new Event(19, "large_packet_mode", "large_packet_mode", "status_ok", "status_ok"), //
                new Event(20, "status_ok", "status_ok") //
        );
        MtlFormula status_valid = new DefaultAtomicProposition("status_valid", "status_valid");
        MtlFormula large_packet_mode = new DefaultAtomicProposition("large_packet_mode", "large_packet_mode");
        MtlFormula long_frame_received = new DefaultAtomicProposition("long_frame_received", "long_frame_received");
        MtlFormula long_frame_error = new DefaultAtomicProposition("long_frame_error", "long_frame_error");
        MtlFormula status_ok = new DefaultAtomicProposition("status_ok", "status_ok");
        MtlFormula transfer_stopped = new DefaultAtomicProposition("transfer_stopped", "transfer_stopped");

        MtlFormula phi = G( //
                IMPLY(//
                        AND(AND(status_valid, NOT(large_packet_mode)), long_frame_received), //
                        OR(AND(long_frame_error, NOT(status_ok)), transfer_stopped)));

        compareLtl_G_R_newU_Mtl(trace, phi,
                "[(12, large_packet_mode=large_packet_mode), (12, long_frame_error=long_frame_error), (12, long_frame_received=long_frame_received), (12, status_ok=status_ok), (12, status_valid=status_valid), (12, transfer_stopped=transfer_stopped)]");
    }

    private void compareLtl_G_R_newU_Mtl(List<IEvent> trace, MtlFormula phi, String expected) {
        System.out.println(phi.toString());
        LtlNnf nnfG = LtlNnf.fromMtlFormula(phi, true, false);
        LtlNnf nnfR = LtlNnf.fromMtlFormula(phi, true, true);

        List<String> causes1 = LtlCausalityChecker.computeCauses(trace, nnfG, false, false).stream()
                .map(c -> c.toString()).sorted().collect(Collectors.toList());
        List<String> causes2 = LtlCausalityChecker.computeCauses(trace, nnfR, false, false).stream()
                .map(c -> c.toString()).sorted().collect(Collectors.toList());
        List<String> causes3 = LtlCausalityChecker.computeCauses(trace, nnfG, true, false).stream()
                .map(c -> c.toString()).sorted().collect(Collectors.toList());
        List<String> causes4 = LtlCausalityChecker.computeCauses(trace, nnfR, true, false).stream()
                .map(c -> c.toString()).sorted().collect(Collectors.toList());

        MtlFormula nnfMtl = MtlNnf.toNnf(phi, false);
        RecursiveMemoizationCausalityChecker rmcc = new RecursiveMemoizationCausalityChecker();
        rmcc.check(trace, nnfMtl, ImmutableSet.of(), VerificationSemantics.Finite);
        List<String> causes5 = rmcc.getCauses().stream().map(c -> c.toString()).sorted().collect(Collectors.toList());

        for (String s: Stream.of(causes1, causes2, causes3, causes4, causes5).map(Object::toString)
                .collect(Collectors.toList()))
        {
            System.out.println(s);
        }

        assertEquals(expected, causes1.toString());
        assertEquals(causes1, causes2);
        assertEquals(causes1, causes3);
        assertEquals(causes1, causes4);
        assertEquals(causes1, causes5);
    }
}
