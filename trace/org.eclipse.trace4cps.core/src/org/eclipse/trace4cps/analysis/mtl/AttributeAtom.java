/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl;

import java.util.Objects;

import org.eclipse.trace4cps.core.ClaimEventType;

public class AttributeAtom {
    String check;

    Integer qValue;

    ClaimEventType type;

    String key;

    String val;

    public AttributeAtom(String check, Integer qValue, ClaimEventType type, String key, String val) {
        this.check = check;
        this.qValue = qValue;
        this.type = type;
        this.key = key;
        this.val = val;
    }

    public String getCheck() {
        return check;
    }

    public Integer getqValue() {
        return qValue;
    }

    public ClaimEventType getType() {
        return type;
    }

    public String getKey() {
        return key;
    }

    public String getVal() {
        return val;
    }

    @Override
    public String toString() {
        return "AttributeAtom [check=" + check + ", qValue=" + qValue + ", type=" + type + ", key=" + key + ", val="
                + val + "]";
    }

    @Override
    public int hashCode() {
        return Objects.hash(check, key, qValue, type, val);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AttributeAtom)) {
            return false;
        }
        AttributeAtom other = (AttributeAtom)obj;
        return Objects.equals(check, other.check) && Objects.equals(key, other.key)
                && Objects.equals(qValue, other.qValue) && type == other.type && Objects.equals(val, other.val);
    }

    public String getLabel() {
        return (type == ClaimEventType.START ? "start " : type == ClaimEventType.END ? "end " : "") + "{'" + key + "'='"
                + val + "'}";
    }
}
