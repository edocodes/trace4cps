/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl.causality

import org.eclipse.xtend.lib.annotations.Data
import org.eclipse.trace4cps.analysis.mtl.AtomicProposition

@Data class Cause {
    int stateIndex;

    AtomicProposition AP;

    override toString() {
        if (AP.attributes.size > 1) throw new IllegalStateException
        val ap = AP.attributes.entrySet.get(0)
        String.format("(%d, %s=%s)", stateIndex, ap.key, ap.value)
    }

    def equalsDeep(Cause other) {
        return this.stateIndex.equals(other.stateIndex) && this.AP.type.equals(other.AP.type) &&
            this.AP.attributes.equals(other.AP.attributes);
    }
}
