/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl.causality

import com.google.common.collect.Streams
import java.util.HashMap
import java.util.Map
import java.util.stream.Stream
import org.eclipse.trace4cps.analysis.mtl.AtomicProposition
import org.eclipse.trace4cps.analysis.mtl.DefaultAtomicProposition
import org.eclipse.trace4cps.analysis.mtl.MtlFormula
import org.eclipse.trace4cps.analysis.mtl.impl.MTLand
import org.eclipse.trace4cps.analysis.mtl.impl.MTLimply
import org.eclipse.trace4cps.analysis.mtl.impl.MTLnext
import org.eclipse.trace4cps.analysis.mtl.impl.MTLnot
import org.eclipse.trace4cps.analysis.mtl.impl.MTLor
import org.eclipse.trace4cps.analysis.mtl.impl.MTLtrue
import org.eclipse.trace4cps.analysis.mtl.impl.MTLuntil
import org.eclipse.trace4cps.core.ClaimEventType
import org.eclipse.xtend.lib.annotations.Data

import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.*

abstract class LtlNnf {
    def static LtlNnf fromMtlFormula(MtlFormula phi, boolean splitAPs) {
        fromMtlFormula_(phi, splitAPs, false);
    }

    def static LtlNnf fromMtlFormula(MtlFormula phi, boolean splitAPs, boolean release) {
        fromMtlFormula_(phi, splitAPs, release);
    }

    def private static LtlNnf fromMtlFormula_(MtlFormula phi, boolean splitAPs, boolean release) {
        switch phi {
            MTLtrue:
                new NnfTrue
            AtomicProposition case splitAPs: {
                // {a, b} ≡ {a} ∧ {b}
                phi.attributes.entrySet //
                .map[a|new NnfAP(phi.type, a.key, a.value)] //
                .reduce([LtlNnf a, LtlNnf b|new NnfAnd(a, b)])
            }
            AtomicProposition case !splitAPs: {
                new NnfAPMany(phi.type, phi.attributes)
            }
            MTLor:
                new NnfOr(fromMtlFormula_(phi.left, splitAPs, release), fromMtlFormula_(phi.right, splitAPs, release))
            MTLand:
                new NnfAnd(fromMtlFormula_(phi.left, splitAPs, release), fromMtlFormula_(phi.right, splitAPs, release))
            MTLimply:
                // a → b ≡ ¬a ∨ b
                new NnfOr(fromMtlFormula_(NOT(phi.left), splitAPs, release),
                    fromMtlFormula_(phi.right, splitAPs, release))
            MTLnext:
                new NnfNext(fromMtlFormula_(phi.child, splitAPs, release))
            MTLuntil: {
                if (!phi.interval.isTrivial) throw new IllegalArgumentException("Only LTL formulas are supported")
                new NnfUntil(fromMtlFormula_(phi.left, splitAPs, release),
                    fromMtlFormula_(phi.right, splitAPs, release))
            }
            MTLnot: {
                val p = phi.child
                switch p {
                    MTLtrue: {
                        new NnfNot(new NnfTrue)
                    }
                    AtomicProposition case splitAPs: {
                        // !{a, b} ≡ !{a} ∨ !{b}
                        p.attributes.entrySet //
                        .map[a|new NnfNot(new NnfAP(p.type, a.key, a.value))] //
                        .reduce([LtlNnf a, LtlNnf b|new NnfOr(a, b)])
                    }
                    AtomicProposition case !splitAPs: {
                        new NnfNot(new NnfAPMany(p.type, p.attributes))
                    }
                    MTLor: {
                        // ¬(a ∨ b) ≡ ¬a ∧ ¬b
                        new NnfAnd(fromMtlFormula_(NOT(p.left), splitAPs, release),
                            fromMtlFormula_(NOT(p.right), splitAPs, release))
                    }
                    MTLand: {
                        // ¬(a ∧ b) ≡ ¬a ∨ ¬b
                        new NnfOr(fromMtlFormula_(NOT(p.left), splitAPs, release),
                            fromMtlFormula_(NOT(p.right), splitAPs, release))
                    }
                    MTLnot:
                        // ¬¬a ≡ a
                        fromMtlFormula_(p.child, splitAPs, release)
                    MTLimply: {
                        // ¬(a → b) ≡ a ∧ ¬b
                        new NnfAnd(fromMtlFormula_(p.left, splitAPs, release),
                            fromMtlFormula_(NOT(p.right), splitAPs, release))
                    }
                    MTLnext: {
                        new NnfWeakNext(fromMtlFormula_(NOT(p.child), splitAPs, release))
                    }
                    MTLuntil case !release && p.left instanceof MTLtrue: {
                        // ¬(⊤ U p) ≡ G¬p (and also ¬(⊤ U ¬p) ≡ G¬¬p ≡ Gp) 
                        // This case (as separate from the Until case below) is not actually necessary to obtain a NNF formula,
                        // but the C algorithm needs a formula of this form to be expressed using Globally in order to not
                        // under-approximate the causes.
                        if (!(phi.child as MTLuntil).interval.isTrivial)
                            throw new IllegalArgumentException("Only LTL formulas are supported")
                        val a = (phi.child as MTLuntil).right
                        new NnfGlobally(fromMtlFormula_(NOT(a), splitAPs, release))
                    }
                    MTLuntil case !release && p.left instanceof MTLnot && (p.left as MTLnot).child instanceof MTLnot: {
                        // ¬(¬¬a U b) ≡ ¬(a U b) 
                        // This case makes sure that the previous case (¬(⊤ U p) ≡ G¬p) works even if the ⊤ is obscured
                        // by double negations
                        val a = ((p.left as MTLnot).child as MTLnot).child
                        fromMtlFormula_(NOT(U(a, p.right, p.interval)), splitAPs, release)
                    }
                    MTLuntil case !release: {
                        // ¬(a U b) ≡ (¬b U (¬a ∧ ¬b)) ∨ G¬b
                        if (!p.interval.isTrivial) throw new IllegalArgumentException("Only LTL formulas are supported")
                        new NnfOr(
                            new NnfUntil(
                                fromMtlFormula_(NOT(p.right), splitAPs, release),
                                new NnfAnd(
                                    fromMtlFormula_(NOT(p.left), splitAPs, release),
                                    fromMtlFormula_(NOT(p.right), splitAPs, release)
                                )
                            ),
                            new NnfGlobally(fromMtlFormula_(NOT(p.right), splitAPs, release))
                        )
                    }
                    MTLuntil case release: {
                        // ¬(a U b) ≡ ¬a R ¬b
                        if (!p.interval.isTrivial) throw new IllegalArgumentException("Only LTL formulas are supported")
                        new NnfRelease(
                            fromMtlFormula(NOT(p.left), splitAPs, release),
                            fromMtlFormula(NOT(p.right), splitAPs, release)
                        )
                    }
                    default:
                        throw new Exception("Unsupported formula")
                }
            }
            default:
                throw new Exception("Unsupported formula")
        }
    }

    def abstract Stream<LtlNnf> getSubformulas()
}

abstract class NnfFormulaLeaf extends LtlNnf {
}

abstract class NnfFormulaAP extends NnfFormulaLeaf {
    def abstract AtomicProposition toMtlAP()
}

abstract class NnfFormulaNode extends LtlNnf {
}

@Data class NnfTrue extends NnfFormulaLeaf {
    override getSubformulas() {
        Stream.of(this)
    }

    override toString() {
        "true"
    }
}

@Data class NnfAP extends NnfFormulaAP {
    ClaimEventType type
    String key
    String value

    new(String key, String value) {
        this(null, key, value)
    }

    new(ClaimEventType type, String key, String value) {
        this.type = type
        this.key = key
        this.value = value
    }

    override getSubformulas() {
        Stream.of(this)
    }

    override AtomicProposition toMtlAP() {
        new DefaultAtomicProposition(type, key, value)
    }

    override toString() {
        String.format("%s{%s=%s}", (type == ClaimEventType.START ? "start " : type == ClaimEventType.END ? "end " : ""),
            key, value)
    }
}

@Data class NnfAPMany extends NnfFormulaAP {
    ClaimEventType type
    Map<String, String> attributes

    new(String... attributes) {
        this(null, attributes)
    }

    new(ClaimEventType type, String... attributes) {
        this.type = type
        this.attributes = new HashMap
        if (attributes !== null) {
            for (var i = 0; i < attributes.length; i += 2) {
                this.attributes.put(attributes.get(i), attributes.get(i + 1))
            }
        }
    }

    new(ClaimEventType type, Map<String, String> attributes) {
        this.type = type
        this.attributes = attributes
    }

    override getSubformulas() {
        Stream.of(this)
    }

    override AtomicProposition toMtlAP() {
        new DefaultAtomicProposition(type, this.attributes)
    }

    override toString() {
        String.format("%s%s", (type == ClaimEventType.START ? "start " : type == ClaimEventType.END ? "end " : ""),
            this.attributes)
    }
}

@Data class NnfNot extends NnfFormulaNode {
    NnfFormulaLeaf child

    override getSubformulas() {
        Stream.concat(child.subformulas, Stream.of(this))
    }

    override toString() {
        "not " + child
    }
}

@Data class NnfNext extends NnfFormulaNode {
    LtlNnf child

    override getSubformulas() {
        Stream.concat(child.subformulas, Stream.of(this))
    }

    override toString() {
        "X " + child
    }
}

@Data class NnfWeakNext extends NnfFormulaNode {
    LtlNnf child

    override getSubformulas() {
        Stream.concat(child.subformulas, Stream.of(this))
    }

    override toString() {
        "X̄ " + child
    }
}

@Data class NnfAnd extends NnfFormulaNode {
    LtlNnf left
    LtlNnf right

    override getSubformulas() {
        Streams.concat(left.subformulas, right.subformulas, Stream.of(this))
    }

    override toString() {
        String.format("(%s and %s)", left, right)
    }
}

@Data class NnfOr extends NnfFormulaNode {
    LtlNnf left
    LtlNnf right

    override getSubformulas() {
        Streams.concat(left.subformulas, right.subformulas, Stream.of(this))
    }

    override toString() {
        String.format("(%s or %s)", left, right)
    }
}

@Data class NnfGlobally extends NnfFormulaNode {
    LtlNnf child

    override getSubformulas() {
        Stream.concat(child.subformulas, Stream.of(this))
    }

    override toString() {
        "G " + child
    }
}

@Data class NnfUntil extends NnfFormulaNode {
    LtlNnf left
    LtlNnf right

    override getSubformulas() {
        Streams.concat(left.subformulas, right.subformulas, Stream.of(this))
    }

    override toString() {
        String.format("(%s U %s)", left, right)
    }
}

@Data class NnfRelease extends NnfFormulaNode {
    LtlNnf left
    LtlNnf right

    override getSubformulas() {
        Streams.concat(left.subformulas, right.subformulas, Stream.of(this))
    }

    override toString() {
        String.format("(%s R %s)", left, right)
    }
}
