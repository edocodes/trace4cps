/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl.causality

import com.google.common.collect.BiMap
import com.google.common.collect.HashBiMap
import java.util.Collection
import java.util.Collections
import java.util.HashSet
import java.util.List
import java.util.Set
import org.eclipse.trace4cps.analysis.mtl.AtomicProposition
import org.eclipse.trace4cps.analysis.mtl.ExplanationTable
import org.eclipse.trace4cps.analysis.mtl.InformativePrefix
import org.eclipse.trace4cps.analysis.mtl.MtlFormula
import org.eclipse.trace4cps.analysis.mtl.MtlUtil
import org.eclipse.trace4cps.analysis.mtl.State
import org.eclipse.trace4cps.analysis.mtl.VerificationSemantics
import org.eclipse.trace4cps.analysis.mtl.check.CompactExplanationTableImpl
import org.eclipse.trace4cps.analysis.mtl.check.SingleFormulaChecker
import org.eclipse.trace4cps.analysis.mtl.check.TabularExplanationTable
import org.eclipse.trace4cps.analysis.mtl.impl.MTLand
import org.eclipse.trace4cps.analysis.mtl.impl.MTLimply
import org.eclipse.trace4cps.analysis.mtl.impl.MTLnext
import org.eclipse.trace4cps.analysis.mtl.impl.MTLnot
import org.eclipse.trace4cps.analysis.mtl.impl.MTLor
import org.eclipse.trace4cps.analysis.mtl.impl.MTLtrue
import org.eclipse.trace4cps.analysis.mtl.impl.MTLuntil
import org.eclipse.trace4cps.analysis.stl.StlFormula

import static org.eclipse.trace4cps.analysis.mtl.check.TabularExplanationTable.MAYBE
import static org.eclipse.trace4cps.analysis.mtl.check.TabularExplanationTable.NO
import static org.eclipse.trace4cps.analysis.mtl.check.TabularExplanationTable.STILLNO
import static org.eclipse.trace4cps.analysis.mtl.check.TabularExplanationTable.STILLYES
import static org.eclipse.trace4cps.analysis.mtl.check.TabularExplanationTable.UNKNOWN
import static org.eclipse.trace4cps.analysis.mtl.check.TabularExplanationTable.YES

final class RecursiveMemoizationCausalityChecker implements SingleFormulaChecker {
    List<? extends State> trace

    Set<InformativePrefix> log

    TabularExplanationTable l

    final BiMap<MtlFormula, Integer> idMap = HashBiMap.create()

    final Set<Cause> causes = new HashSet

    override InformativePrefix check(List<? extends State> trace, MtlFormula phi, Set<InformativePrefix> log,
        VerificationSemantics sem) {
        this.trace = trace
        if (log !== null) {
            this.log = log
        } else {
            this.log = Collections.emptySet
        }
        if (sem != VerificationSemantics.Finite)
            throw new Exception("The checker with causality only supports finite semantics");

        if (!MtlNnf.isNnf(phi))
            throw new IllegalArgumentException("Formula must be in negative normal form");

        var List<MtlFormula> sub = MtlUtil.getSubformulas(phi)
        idMap.clear()
        var int i = 0
        for (MtlFormula f : sub) {
            idMap.put(f, i)
            i++
        }
        this.l = new CompactExplanationTableImpl(trace, idMap)

        // Now compute the truth value of the top-level formula depth first
        // filling needed table entries on-the-fly:
        eval(phi, 0, true)
        var InformativePrefix result = l.getValue(phi, 0)
        if (!this.log.contains(result)) {
            // throw away the explanation
            l = null
        }
        return result
    }

    override void checkOnAllEvents(List<? extends State> trace, Collection<AtomicProposition> phis) {
        throw new IllegalStateException("Not supported in this class")
    }

    /** 
     * {@inheritDoc}
     */
    override ExplanationTable getExplanation() {
        l
    }

    def Set<Cause> getCauses() {
        causes
    }

    def private int eval(MtlFormula f, int i, boolean recordCauses) {
        switch f {
            MTLtrue:
                evalTrue(f, i)
            AtomicProposition:
                evalAP(f, i, recordCauses)
            MTLnot:
                switch f.child {
                    MTLtrue:
                        evalFalse(f, i)
                    AtomicProposition:
                        evalNotAP(f, i, recordCauses)
                    default:
                        throw new IllegalStateException('''Formula should always be in NNF here!''')
                }
            MTLand:
                evalAnd(f, i, recordCauses)
            MTLimply:
                throw new IllegalArgumentException("implies should not occur in NNF formula")
            MTLor:
                evalOr(f, i, recordCauses)
            MTLnext:
                evalNext(f, i, recordCauses)
            MTLweaknext:
                evalWeakNext(f, i, recordCauses)
            MTLuntil:
                evalUntil(f, i, recordCauses)
            MTLrelease:
                evalRelease(f, i, recordCauses)
            StlFormula:
                throw new IllegalArgumentException("STL not supported in this class")
            default:
                throw new IllegalStateException('''use of «f.class» constructs unsupported!''')
        }
    }

    def private int evalTrue(MTLtrue f, int i) {
        val formulaIndex = idMap.get(f)
        return l.put(formulaIndex, i, YES)
    }

    def private int evalFalse(MTLnot f, int i) {
        val childIndex = idMap.get(f.child)
        val formulaIndex = idMap.get(f)
        var v = l.get(childIndex, i)
        if (v == UNKNOWN) {
            v = evalTrue(f.child as MTLtrue, i)
        }
        return l.put(formulaIndex, i, not(v))
    }

    def private int evalAP(AtomicProposition f, int i, boolean recordCauses) {
        val formulaIndex = idMap.get(f)
        if (trace.get(i).satisfies(f)) {
            return l.put(formulaIndex, i, YES)
        } else {
            if (recordCauses) causes.add(new Cause(i, f))
            return l.put(formulaIndex, i, NO)
        }
    }

    def private int evalNotAP(MTLnot f, int i, boolean recordCauses) {
        val ap = f.child as AtomicProposition
        val formulaIndex = idMap.get(f)

        var v = l.get(idMap.get(f.child), i)
        if (v == UNKNOWN) {
            v = evalAP(ap, i, false)
        }
        if (v == YES) {
            if (recordCauses) causes.add(new Cause(i, ap))
            return l.put(formulaIndex, i, NO)
        } else {
            return l.put(formulaIndex, i, YES)
        }
    }

    def private int evalNext(MTLnext f, int i, boolean recordCauses) {
        if (i == trace.size - 1) {
            return l.put(idMap.get(f), i, NO)
        } else {
            var int v = l.get(idMap.get(f.child), i + 1)
            if (v == UNKNOWN || recordCauses) {
                v = eval(f.child, i + 1, recordCauses)
            }
            return l.put(idMap.get(f), i, v)
        }
    }

    def private int evalWeakNext(MTLweaknext f, int i, boolean recordCauses) {
        if (i == trace.size - 1) {
            return l.put(idMap.get(f), i, YES)
        } else {
            var int v = l.get(idMap.get(f.child), i + 1)
            if (v == UNKNOWN || recordCauses) {
                v = eval(f.child, i + 1, recordCauses)
            }
            return l.put(idMap.get(f), i, v)
        }
    }

    def private int evalAnd(MTLand f, int i, boolean recordCauses) {
        var int v1 = l.get(idMap.get(f.left), i)
        if (v1 == UNKNOWN || recordCauses) {
            v1 = eval(f.left, i, recordCauses)
        }
        var int v2 = l.get(idMap.get(f.right), i)
        if (v2 == UNKNOWN || recordCauses) {
            v2 = eval(f.right, i, recordCauses)
        }

        return l.put(idMap.get(f), i, and(v1, v2))
    }

    def private int evalOr(MTLor f, int i, boolean recordCauses) {
        var int v1 = l.get(idMap.get(f.left), i)
        if (v1 == UNKNOWN || recordCauses) {
            v1 = eval(f.left, i, false)
        }
        var int v2 = l.get(idMap.get(f.right), i)
        if (v2 == UNKNOWN || recordCauses) {
            v2 = eval(f.right, i, false)
        }

        if (v1 == NO && v2 == NO) {
            eval(f.left, i, recordCauses)
            eval(f.right, i, recordCauses)
        }

        return l.put(idMap.get(f), i, or(v1, v2))
    }

    def private int evalUntil(MTLuntil f, int i, boolean recordCauses) {
        val v = evalUntil_(f, i, false)
        if (recordCauses && v == NO) {
            evalUntil_(f, i, true)
        }
        v
    }

    def private int evalUntil_(MTLuntil f, int i, boolean recordCauses) {
        var double ti = trace.get(i).timestamp.doubleValue

        var int formulaIndex = idMap.get(f)
        var int leftIndex = idMap.get(f.left)
        var int rightIndex = idMap.get(f.right)

        var boolean c1 = false
        var boolean c11 = true
        for (var int j = i; j < trace.size; j++) {
            var double tj = trace.get(j).timestamp.doubleValue
            var int r2 = l.get(rightIndex, j)
            if (r2 == UNKNOWN) {
                r2 = eval(f.right, j, false)
            }
            c1 = c1 || (r2 == YES && f.interval.contains(tj - ti) && c11)
            if (c1) {
                return l.put(formulaIndex, i, YES)
            }

            // if t∈I and !r2, add r2 to causes
            if (recordCauses && f.interval.contains(tj - ti) && r2 == NO) {
                eval(f.right, j, true)
            }

            var int r1 = l.get(leftIndex, j)
            if (r1 == UNKNOWN) {
                r1 = eval(f.left, j, false)
            }
            c11 = c11 && (r1 == YES)
            var boolean intervalPassed = f.interval.strictlySmallerThan(tj - ti)

            // if !r2 and !r1, add r1 to causes (r2 is already in causes)
            if (recordCauses && !intervalPassed && r1 == NO) {
                eval(f.left, j, true)
            }

            if ((!c11 || j == trace.size - 1 || intervalPassed) && !c1) {
                return l.put(formulaIndex, i, NO)
            }
        }
        throw new IllegalStateException
    }

    def private int evalRelease(MTLrelease f, int i, boolean recordCauses) {
        val v = evalRelease_(f, i, false)
        if (recordCauses && v == NO) {
            evalRelease_(f, i, true)
        }
        v
    }

    def private int evalRelease_(MTLrelease f, int i, boolean recordCauses) {
        var ti = trace.get(i).timestamp.doubleValue

        var formulaIndex = idMap.get(f)
        var leftIndex = idMap.get(f.left)
        var rightIndex = idMap.get(f.right)

        var boolean c1 = true
        var boolean c11 = false
        for (var j = i; j < trace.size; j++) {
            var tj = trace.get(j).timestamp.doubleValue
            var int r1 = l.get(leftIndex, j)
            if (r1 == UNKNOWN) {
                r1 = eval(f.left, j, false)
            }
            var int r2 = l.get(rightIndex, j)
            if (r2 == UNKNOWN) {
                r2 = eval(f.right, j, false)
            }

            // add r2 and maybe r1 to causes
            if (recordCauses && f.interval.contains(tj - ti) && r2 == NO) {
                eval(f.right, j, true)
                if (r1 == NO) {
                    eval(f.left, j, true)
                }
            }
            // add r1 to causes
            if (recordCauses && j < trace.size - 1 && f.interval.contains(tj - ti) && r1 == NO) {
                eval(f.left, j, true)
            }

            c1 = c1 && (!f.interval.contains(tj - ti) || r2 == YES || c11)
            if (!c1) {
                return l.put(formulaIndex, i, NO)
            }
            c11 = c11 || (r1 == YES)
            var intervalPassed = f.interval.strictlySmallerThan(tj - ti)
            if ((c11 || j == trace.size - 1 || intervalPassed) && c1) {
                return l.put(formulaIndex, i, YES)
            }

        }
        throw new IllegalStateException
    }

//    def private int fillReleaseUnoptimized(MTLrelease f, int i) {
//        var ti = trace.get(i).timestamp.doubleValue
//
//        var formulaIndex = idMap.get(f)
//        var leftIndex = idMap.get(f.left)
//        var rightIndex = idMap.get(f.right)
//
//        var boolean c1 = true
//        var boolean c11 = false
//        for (var int j = i; j < trace.size; j++) {
//            var tj = trace.get(j).timestamp.doubleValue
//            var r1 = l.get(leftIndex, j)
//            if (r1 == UNKNOWN) {
//                r1 = fillTable(f.left, j)
//            }
//            var r2 = l.get(rightIndex, j)
//            if (r2 == UNKNOWN) {
//                r2 = fillTable(f.right, j)
//            }
//
//            c1 = c1 && (!f.interval.contains(tj - ti) || r2 == YES || c11)
//            c11 = c11 || (r1 == YES)
//        }
//        return l.put(formulaIndex, i, c1 ? YES : NO)
//    }
    def private static int not(int v) {
        if (v == YES) {
            return NO
        } else if (v == NO) {
            return YES
        } else if (v == STILLNO) {
            return STILLYES
        } else if (v == STILLYES) {
            return STILLNO
        } else {
            // v == MAYBE
            return MAYBE
        }
    }

    def private static int or(int v1, int v2) {
        if (v1 == YES) {
            return YES
        } else if (v1 == NO) {
            return v2
        } else if (v1 == STILLYES) {
            if (v2 == YES) {
                return YES
            } else {
                return STILLYES
            }
        } else if (v1 == STILLNO) {
            if (v2 == YES || v2 == STILLYES || v2 == MAYBE) {
                return v2
            } else {
                return STILLNO
            }
        } else {
            // v1 == MAYBE
            if (v2 == YES || v2 == STILLYES) {
                return v2
            } else {
                return MAYBE
            }
        }
    }

    def private static int and(int v1, int v2) {
        if (v1 == YES) {
            return v2
        } else if (v1 == NO) {
            return NO
        } else if (v1 == STILLYES) {
            if (v2 == MAYBE || v2 == STILLNO || v2 == NO) {
                return v2
            } else {
                return STILLYES
            }
        } else if (v1 == STILLNO) {
            if (v2 == NO) {
                return NO
            } else {
                return STILLNO
            }
        } else {
            // v1 == MAYBE
            if (v2 == STILLNO || v2 == NO) {
                return v2
            } else {
                return MAYBE
            }
        }
    }
}
