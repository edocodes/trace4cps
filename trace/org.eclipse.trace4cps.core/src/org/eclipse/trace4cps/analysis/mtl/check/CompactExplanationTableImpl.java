/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl.check;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.trace4cps.analysis.mtl.InformativePrefix;
import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.analysis.mtl.State;
import org.eclipse.trace4cps.analysis.mtl.causality.MTLrelease;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLuntil;

import com.google.common.collect.BiMap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.MultimapBuilder;

/**
 * We need 4 bits per index,formula. Optimization for locality in Until computation: put all formulas together
 */
public final class CompactExplanationTableImpl implements TabularExplanationTable {
    private final List<? extends State> trace;

    private final BiMap<MtlFormula, Integer> subForms;

    // Number of 32-bit ints to encode the truth values of 1 state
    private final int p;

    private final int[] table;

    private final ListMultimap<Integer, Region> intervals = MultimapBuilder.hashKeys().arrayListValues().build();

    public CompactExplanationTableImpl(List<? extends State> trace, BiMap<MtlFormula, Integer> subForms) {
        this.trace = trace;
        this.subForms = subForms;
        this.p = (subForms.size() / 8) + 1;
        this.table = new int[p * trace.size()];
        for (int i = 0; i < p * trace.size(); i++) {
            table[i] = 0;
        }
    }

    @Override
    public List<? extends State> getTrace() {
        return trace;
    }

    @Override
    public int get(int formulaIndex, int stateIndex) {
        // unpack value for formula:
//        return (table[stateIndex * P + formulaIndex / 8] >> (4 * (formulaIndex % 8))) & 15;
        return (table[(stateIndex * p) + (formulaIndex >> 3)] >> ((formulaIndex & 7) << 2)) & 15;
    }

    @Override
    public int put(int formulaIndex, int stateIndex, int c) {
//        table[stateIndex * P + formulaIndex / 8] |= (c << (4 * (formulaIndex % 8)));
        table[stateIndex * p + (formulaIndex >> 3)] |= (c << ((formulaIndex & 7) << 2));
        return c;
    }

    @Override
    public List<Region> getRegions(MtlFormula phi) {
        final int formulaIndex = subForms.get(phi);
        List<Region> regions = new ArrayList<Region>();
        // Init the start region:
        InformativePrefix current = getValue(formulaIndex, 0);
        RegionImpl r = new RegionImpl(current);
        r.setStart(0, trace.get(0).getTimestamp().doubleValue());
        // iterate over the table until the value changes:
        for (int i = 1; i < trace.size(); i++) {
            InformativePrefix v = getValue(formulaIndex, i);
            if (v != current) {
                // Close current region:
                r.setEnd(i - 1, trace.get(i - 1).getTimestamp().doubleValue());
                regions.add(r);
                // Open new region:
                current = v;
                r = new RegionImpl(current);
                r.setStart(i, trace.get(i).getTimestamp().doubleValue());
            }
        }
        // Add the last open region:
        r.setEnd(trace.size() - 1, trace.get(trace.size() - 1).getTimestamp().doubleValue());
        regions.add(r);
        return regions;
    }

    @Override
    public InformativePrefix getValue(MtlFormula phi, int index) {
        return getValue(subForms.get(phi), index);
    }

    public InformativePrefix getValue(int formulaIndex, int index) {
        switch (get(formulaIndex, index)) {
            case YES:
                return InformativePrefix.TRUE;
            case NO:
                return InformativePrefix.FALSE;
            case MAYBE:
                return InformativePrefix.NON_INFORMATIVE;
            case STILLYES:
                return InformativePrefix.STILL_TRUE;
            case STILLNO:
                return InformativePrefix.STILL_FALSE;
            case UNKNOWN:
                return InformativePrefix.NOT_COMPUTED;
            default:
                throw new IllegalStateException();
        }
    }

    @Override
    public void addInterval(int formulaIndex, int startIndex, double startTime, int endIndex, double endTime) {
        MtlFormula f = subForms.inverse().get(formulaIndex);
        assert f instanceof MTLuntil || f instanceof MTLrelease;
        intervals.put(formulaIndex, new RegionImpl(startIndex, startTime, endIndex, endTime, null));
    }

    @Override
    public Collection<Region> getIntervals(MtlFormula phi) {
        return intervals.get(subForms.get(phi));
    }

    public int getFormulaIndex(MtlFormula phi) {
        return subForms.get(phi);
    }
}
