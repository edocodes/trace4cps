/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl.check;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.eclipse.trace4cps.analysis.mtl.AtomicProposition;
import org.eclipse.trace4cps.analysis.mtl.ExplanationTable;
import org.eclipse.trace4cps.analysis.mtl.InformativePrefix;
import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.analysis.mtl.State;
import org.eclipse.trace4cps.analysis.mtl.VerificationSemantics;

public interface SingleFormulaChecker {
    InformativePrefix check(List<? extends State> trace, MtlFormula phi, Set<InformativePrefix> log,
            VerificationSemantics sem);

    void checkOnAllEvents(List<? extends State> trace, Collection<AtomicProposition> phis);

    ExplanationTable getExplanation();
}
