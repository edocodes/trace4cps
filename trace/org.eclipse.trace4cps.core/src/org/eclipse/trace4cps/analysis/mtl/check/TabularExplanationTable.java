/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl.check;

import java.util.Collection;

import org.eclipse.trace4cps.analysis.mtl.ExplanationTable;
import org.eclipse.trace4cps.analysis.mtl.InformativePrefix;
import org.eclipse.trace4cps.analysis.mtl.MtlFormula;

public interface TabularExplanationTable extends ExplanationTable {
    public static final int NO = 1;

    public static final int STILLNO = 2;

    public static final int MAYBE = 3;

    public static final int STILLYES = 4;

    public static final int YES = 5;

    public static final int UNKNOWN = 0;

    int get(int formulaIndex, int stateIndex);

    int put(int formulaIndex, int stateIndex, int c);

    InformativePrefix getValue(MtlFormula phi, int index);

    public void addInterval(int formulaIndex, int startIndex, double startTime, int endIndex, double endTime);

    public Collection<Region> getIntervals(MtlFormula phi);
}
