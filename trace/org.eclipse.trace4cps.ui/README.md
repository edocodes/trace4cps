# How to reproduce the Eclipse help status

Some notes on reproducing the screenshots in the Eclipse help documentation.
The etf files to use can be found in the `/org.eclipse.trace4cps.ui/html/content/ODSE-traces.zip` file.
By default, use the `model-bf.etf` file and apply:

_Defaults:_
- Apply  _claim grouping_  based on `name`
- Apply  _claim coloring_  based on `id`
-  _filter resources_  to only keep memory resources (i.e. M1, M2)

TIP: you may want to save this state


## Visualisation

- Apply a  _claim filter_  to only keep claims with `id` [0..5].
- Set  _claim scaling_  to `FULL`.

## Critical path analysis

No additional remarks

## Distance analyis

Select `model-bf.etf`, `model-ff.etf` and `reference.etf` and  _Open TRACE view_  from context menu.

- Apply the defaults as described above.
- Hide dependencies.
- Start  _Order analysis_  in from the toolbar.
- Select the reference.etf

NOTE: The detailed screenshots could not be reproduced exactly, but a fragment should be found that only contains a single difference.

## Resource usage analysis

No additional remarks

## TRACE Little's Law

No additional remarks

## Behavioral analysis

- Clear the resource filter 
- Filter resources, select all and deselect B, M1 and M2

NOTE: There seemed to be a few bugs in this analysis

## Runtime verification

This is a rudimental prototype, but can be reproduced.
Explaining the analysis results needs some attention.
