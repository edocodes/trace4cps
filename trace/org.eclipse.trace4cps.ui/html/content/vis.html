<!--

    Copyright (c) 2021 Contributors to the Eclipse Foundation

    This program and the accompanying materials are made
    available under the terms of the Eclipse Public License 2.0
    which is available at https://www.eclipse.org/legal/epl-2.0/

    SPDX-License-Identifier: EPL-2.0

-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 5.0 Transitional//EN">

<html>
<head>
    <link rel="stylesheet" type="text/css" href="../css/style.css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <title>TRACE - Visualisation</title>
</head>
<div>
<h1>TRACE Visualisation</h1>

<h2>Opening a TRACE view</h2>
<p>
TRACE files (<code>.etf</code> files according to the format specified <a href="format.html">here</a>)
can be viewed by double-clicking them in the Project Explorer view.
Multiple TRACE files can be opened in the same view by selecting them in the Project Explorer view,
and then right-clicking the selection. In the context menu then select the "Open as TRACE view" item.
Both ways of opening TRACE files result in a Gantt-chart view in the IDE.
</p>
<p>
All TRACE entities (claims, events, signals and dependencies) are visualized in a chart with on the horizontal axis the time,
and on the vertical axis a number of labeled <i>sections</i>.
A single section contains a set of claims, a set of events or a single signal.
The <i>grouping</i> configuration (see below) determines how the claims and events are distributed over sections. 
The example in Figure 1 shows a part of a trace with claims and dependencies between claims.
(It is a view on the "model-bf.etf" trace of the running example).
</p>

<figure>
<a target="_blank" href="../img/trace-1.png"><img src="../img/trace-1.png" class="open600" /> </a>
<figcaption>Figure 1: A fragment of an example trace from the <a href="example.html">running example</a>.</figcaption>
</figure>

<h2>Basic viewing actions</h2>

<h3>Zooming and panning</h3>
<p>
Panning is done by Alt-down and dragging the chart.
</p
<p>
Scrolling can be done as follows:
<ul>
<li> Mouse wheel:
    <ul>
    <li>Scrolls on the horizontal axis (time) if no vertical scrollbar is present.</li>
    <li>Scrolls on the vertical axis if a vertical scrollbar is present.</li>
    </ul>
</li>
<li> The arrows on the scroll bars can be used for scrolling in smaller steps. </li>
</ul>
Zooming can be done in various ways:
<ul>
<li> Right-click the chart: The context menu has several items to zoom one or both of the axes.</li>
<li> Ctrl-down and mouse wheel zooms the horizontal axis in or out. </li>
<li> Shft-Ctrl-down and mouse wheel zooms the vertical axis in or out. </li>
<li> Area selection in the chart using the mouse zooms to that selection. </li>
<li> A mouse dragging gesture to the left zooms out fully on both axes.</li>
</ul>
</p>

<h3>Selection and tooltips</h3>
Selection is supported for all concepts: claims, events, dependencies and signals.
The Eclipse Properties view shows the properties of the selected item.
The selected item is shown with a yellow border.
Tool tips are also supported for all four concepts.
Sometimes it is unclear which points can be selected in a signal.
Therefore, these points can be explicitly shown (see below). 

<h2>Measurement tool</h2>
<p>
The measurement functionality can be used to measure the time between claims, events and signal segments.
By hovering over such an entity, a vertical marker is shown.
By a left-click of the mouse with Alt-down, a marker is set.
Setting a second marker adds an annotation to the chart with the distance between the markers.
Multiple markers can be defined, see Figure 2.
Note that the annotation label can be dragged vertically.
<figure>
<a target="_blank" href="../img/trace-measurement.png"><img src="../img/trace-measurement.png" class="open600" /> </a>
<figcaption>Figure 2: Adding time measurements between claims.</figcaption>
</figure>

</p>

<h2>Configuring the view</h2>
The view can be configured via a number of menus on the top right of the view part in the UI:
<ul>
<li> <img src="../../icons/synced.png" /> switches between the activity and resource views</li>
<li> <img src="../../icons/keygroups_obj.png" /> switches between FULL and NONE scaling of the claims (only in activity view)</li>
<li> <img src="../../icons/XSDSequence.gif" /> toggles whether signal markers are shown</li>
<li> <img src="../../icons/xml_text_node.png" /> selects the describing attributes for events, dependencies, claims, resources, and signals</li>
<li> <img src="../../icons/palette_view.gif" /> selects the coloring attributes for events, claims, and dependencies</li>
<li> <img src="../../icons/category_obj.png" /> selects the grouping attributes for events and claims (only in activity view)</li>
<li> <img src="../../icons/pin_view.png" /> completely hides parts of the trace</li>
<li> <img src="../../icons/filter_ps.png" /> configures filters</li>
<li> <img src="../../icons/delete.png" /> clears filters or extensions</li>
<li> <img src="../../icons/save_edit.png" /> persists the current view</li>
</ul>
Each of these functionalities is explained below.
Next to these items in the menu bar, there are other items that trigger analysis methods. These are explained
in other sections.

<h2>Activity vs resource view: <img src="../../icons/synced.png" /></h2>
The distinction between activity and resource view is only about claims.
In the resource view, the grouping is done according to the resource of a claim.
I.e., all claims that use the same resource are in the same section.
Furthermore, the scaling of each claim in the swimlane (i.e., its offset and height) is done according to the claimed amount and offset of the claim.
In the activity view, the grouping of the claims is done according to the grouping attributes that can be defined by the user (see below).
Figures 1 and 2 show the activity view, and Figure 3 shows the resource view (note that Figure 2 only shows
the memory resources).

<figure>
<a target="_blank" href="../img/vis-1.png"><img src="../img/vis-1.png" class="open600" /> </a>
<figcaption>Figure 3: The resource view of the trace in Figure 1.</figcaption>
</figure>


<h2>Scaling of claims: <img src="../../icons/keygroups_obj.png" /></h2>
In the activity view the user can choose whether the scaling of claims is NONE or FULL.
The former is the default value as it is more efficient than the FULL option.
The difference between the options shows if there are claims in the same section that have an overlap in their time domain.
With the NONE option, the claims are just drawn on top of eachother, which may hamper detailed inspection.
With the FULL option, overlapping claims are stacked to fill the swimlane.
In that way, overlap is made explicit, but the efficiency of the visualization may suffer.

<h2>Signal markers: <img src="../../icons/XSDSequence.gif" /></h2>
The user can toggle the visualization of signal markers.
There is a marked point in the signal for every polynomial fragment in the signal.
Selecting such a point shows the properties of the fragment in the properties view.  

<h2>Description: <img src="../../icons/xml_text_node.png" /></h2>
<p>
This menu lets the user select the attributes that are used, e.g., for tooltips and section labels.
The selected attributes have the following effects for the different trace parts: 
<ul>
<li>
Events: The describing attributes are used for the tooltip and section label.
</li>
<li>
Dependencies: The describing attributes are used for the tooltip.
</li>
<li>
Claims: The describing attributes are used for the tooltip and section label. Furthermore, the checkbox
<i>show claim descriptions</i> can be used to (de)activate labels based on the describing
attributes inside the claims (if there is enough space to render the label).
</li>
<li>
Resources: The describing attributes are used for the section label if in
<i>resource view</i>.
</li>
<li>
Signals: The describing attributes are used for the tooltip and section label.
</li>
</ul>
</p>

<h2>Coloring: <img src="../../icons/palette_view.gif" /></h2>
<p>
The user can specify a subset of attributes for coloring claims, events and dependencies.
These attributes are used to compute a <i>coloring partition</i>, i.e., a partition of the claims, events or
dependencies into a set of disjoint <i>cells</i> such that all elements of a cell have the same attributes and values.
All elements in a cell will have the same color.
The current TRACE palette has approximately 40 colors, and each cell of the coloring partition is mapped onto this palette.
In the figures above, we have used the "id" attribute for coloring.
</p>
<p>
There is also the option to explicitly specify the color of a claim, event or dependency.
This is done by assigning an attribute named "color" with one of the following values:
<ul>
<li>red</li>
<li>blue</li>
<li>green</li>
<li>yellow</li>
<li>magenta</li>
<li>cyan</li>
<li>pink</li>

<li>dark_red</li>
<li>dark_blue</li>
<li>dark_green</li>
<li>dark_yellow</li>
<li>dark_magenta</li>
<li>dark_cyan</li>

<li>light_red</li>
<li>light_blue</li>
<li>light_green</li>
<li>light_yellow</li>
<li>light_magenta</li>
<li>light_cyan</li>

<li>very_dark_red</li>
<li>very_dark_blue</li>
<li>very_dark_green</li>
<li>very_dark_yellow</li>
<li>very_dark_magenta</li>
<li>very_dark_cyan</li>

<li>very_light_red</li>
<li>very_light_blue</li>
<li>very_light_green</li>
<li>very_light_yellow</li>
<li>very_light_magenta</li>
<li>very_light_cyan</li>

<li>turquoise</li>
<li>orange</li>
<li>sienna</li>
<li>purple</li>
<li>dark_gray</li>

<li>half_blue</li>
<li>red_pure</li>
<li>light_gray</li>
<li>gray</li>
<li>black</li>
<li>white</li>

<li>red_shade_00</li>
<li>red_shade_01</li>
<li>red_shade_02</li>
<li>red_shade_03</li>
<li>red_shade_04</li>
<li>red_shade_05</li>
<li>red_shade_06</li>
<li>red_shade_07</li>
<li>red_shade_08</li>
<li>red_shade_09</li>
<li>red_shade_10</li>
</ul>
<b>Note:</b> The special coloring attribute is only used if no coloring attributes are specified. 
</p>

<h2>Grouping: <img src="../../icons/category_obj.png" /></h2>
The user can specify a subset of attributes for grouping claims and events into the various sections on
the vertical axis.
These attributes are used to compute a <i>grouping partition</i>, i.e., a partition of the claims or events
such that all elements of a cell have the same attributes and values.
All elements in a cell are shown in the same section.
The label of the section is created from the values of the grouping attributes.
In Figure 1 above we have used the "name" attribute for grouping. Note that the grouping configuration
is not active in the resource view, because then each resource has its own section. 

<h2>Hiding: <img src="../../icons/pin_view.png" /></h2>
The user can hide and show all claims, events, dependencies and signals using the buttons in this menu.
If certain aspects are hidden with this menu, then they will still be in the scope of the various analysis
algorithms that TRACE provides.

<h2>Filtering: <img src="../../icons/filter_ps.png" /></h2>
The hiding menu gives a very coarse filter. Using the filtering menu, the user can select a number of attributes
and values of these attributes that are included in the view.
If a filter was already defined, then the conjunction of the two specifications is taken.
In the figures above we have filtered on the "id" attribute and only included values 0 to 5.
<p>
If certain elements are filtered out, then they will <b>NOT</b> be in the scope of the various analysis
algorithms that TRACE provides.
</p>

<h2>Clearing filters and extensions: <img src="../../icons/delete.png" /></h2>
With this menu all or parts of defined filters can be cleared. Furthermore, there are several analysis
methods that add information to the trace (e.g., dependencies or signals). These can also be cleared
with this  menu. 

<h2>View persistence: <img src="../../icons/save_edit.png" /></h2>
The configuration of the current view can be persisted with the save button.
When a trace is close and opened again, or when the Eclipse IDE is restarted, the view is restored according to the persisted
view configuration.
Note that the visualization of analysis results is typically not saved.

</div>
</html>