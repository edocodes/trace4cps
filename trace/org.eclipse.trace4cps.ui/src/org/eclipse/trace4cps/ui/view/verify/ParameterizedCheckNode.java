/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.verify;

import java.text.DecimalFormat;
import java.text.Format;

import org.eclipse.trace4cps.analysis.mtl.InformativePrefix;
import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.analysis.mtl.MtlResult;
import org.eclipse.trace4cps.tl.VerificationResult;
import org.eclipse.trace4cps.tl.etl.Formula;
import org.eclipse.trace4cps.ui.view.TraceView;

public class ParameterizedCheckNode implements IExplainableNode {
    private static final Format FORMAT = new DecimalFormat("0000");

    private final TraceView view;

    private final MtlFormula phi;

    private final VerificationResult res;

    private final int quantValue;

    private final String label;

    private final String checkName;

    private FormulaNode formulaNode;

    public ParameterizedCheckNode(MtlFormula phi, VerificationResult res, Formula etl, TraceView v) {
        this.phi = phi;
        this.res = res;
        this.view = v;
        this.quantValue = res.getQuantifierValue(phi).intValue();
        this.label = res.getName(phi) + "(" + FORMAT.format(this.quantValue) + ")";
        this.checkName = String.format("%s(%d)", res.getName(phi), this.quantValue);
        this.formulaNode = new FormulaNode(etl, phi, phi, res.getQuantName(phi), res, view);
    }

    @Override
    public TraceView getTraceView() {
        return view;
    }

    public Integer getQuantValue() {
        return quantValue;
    }

    public String getLabel() {
        return label;
    }

    @Override
    public String getCheckName() {
        return checkName;
    }

    @Override
    public MtlFormula getFormula() {
        return phi;
    }

    public MtlResult getMTLresult() {
        return res.getResult(phi);
    }

    @Override
    public VerificationResult getResult() {
        return res;
    }

    public InformativePrefix informative() {
        return res.getResult(phi).informative();
    }

    public FormulaNode getFormulaNode() {
        return this.formulaNode;
    }

    @Override
    public String getExplanationLabel() {
        return formulaNode.getExplanationLabel();
    }

    @Override
    public MtlFormula getCheck() {
        return phi;
    }
}
