/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.verify;

import java.awt.Color;
import java.awt.Paint;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.ITreeViewerListener;
import org.eclipse.jface.viewers.TreeExpansionEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.trace4cps.analysis.mtl.AtomicProposition;
import org.eclipse.trace4cps.analysis.mtl.AttributeAtom;
import org.eclipse.trace4cps.analysis.mtl.DefaultAtomicProposition;
import org.eclipse.trace4cps.analysis.mtl.ExplanationTable;
import org.eclipse.trace4cps.analysis.mtl.ExplanationTable.Region;
import org.eclipse.trace4cps.analysis.mtl.InformativePrefix;
import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.analysis.mtl.MtlResult;
import org.eclipse.trace4cps.analysis.mtl.MtlUtil;
import org.eclipse.trace4cps.analysis.mtl.State;
import org.eclipse.trace4cps.analysis.mtl.VerificationSemantics;
import org.eclipse.trace4cps.analysis.mtl.causality.Cause;
import org.eclipse.trace4cps.analysis.mtl.causality.LtlCausalityChecker;
import org.eclipse.trace4cps.analysis.mtl.causality.LtlNnf;
import org.eclipse.trace4cps.analysis.mtl.causality.MtlNnf;
import org.eclipse.trace4cps.analysis.mtl.causality.RecursiveMemoizationCausalityChecker;
import org.eclipse.trace4cps.analysis.mtl.check.TabularExplanationTable;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLnot;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLuntil;
import org.eclipse.trace4cps.analysis.signal.impl.PsopHelper;
import org.eclipse.trace4cps.analysis.stl.StlFormula;
import org.eclipse.trace4cps.analysis.stl.impl.StlNeg;
import org.eclipse.trace4cps.analysis.stl.impl.StlTrue;
import org.eclipse.trace4cps.analysis.stl.impl.StlUntil;
import org.eclipse.trace4cps.core.IAttributeAware;
import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.core.TracePart;
import org.eclipse.trace4cps.core.impl.Claim;
import org.eclipse.trace4cps.core.impl.ClaimEvent;
import org.eclipse.trace4cps.core.impl.Event;
import org.eclipse.trace4cps.core.impl.ModifiableTrace;
import org.eclipse.trace4cps.core.impl.Resource;
import org.eclipse.trace4cps.tl.FormulaHelper;
import org.eclipse.trace4cps.tl.VerificationResult;
import org.eclipse.trace4cps.tl.etl.Formula;
import org.eclipse.trace4cps.ui.view.TraceView;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.osgi.framework.Bundle;

import com.google.common.collect.ImmutableSet;

public class VerificationResultView extends ViewPart
        implements IResourceChangeListener, IDoubleClickListener, IPartListener, IMenuListener, ITreeViewerListener
{
    public static final String VIEW_ID = "org.eclipse.trace4cps.ui.view.verify.VerificationResultView";

    private ResultTree tree = new ResultTree();

    private TreeViewer viewer;

    private Integer lastLaneNumber = 0;

    public static void showView(String specFile, String traceFile, VerificationResult results, TraceView traceView) {
        final IWorkbench workbench = PlatformUI.getWorkbench();
        workbench.getDisplay().asyncExec(new Runnable() {
            @Override
            public void run() {
                try {
                    IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
                    IViewPart view = page.showView(VerificationResultView.VIEW_ID);
                    ((VerificationResultView)view).setResult(specFile, traceFile, results, traceView);
                } catch (CoreException e) {
                    ErrorDialog.openError(null, "ETL Verification Error", "Failed to update result view",
                            e.getStatus());
                }
            }
        });
    }

    private void setResult(String specPath, String tracePath, VerificationResult res, TraceView view) {
        tree.refresh();
        tree.add(tracePath, specPath, res, view);
        tree.refresh();
        viewer.refresh();
        viewer.expandToLevel(3);
    }

    @Override
    public void partActivated(IWorkbenchPart part) {
    }

    @Override
    public void partBroughtToTop(IWorkbenchPart part) {
    }

    @Override
    public void partClosed(IWorkbenchPart part) {
        tree.partClosed(part);
        viewer.refresh();
    }

    @Override
    public void partDeactivated(IWorkbenchPart part) {
    }

    @Override
    public void partOpened(IWorkbenchPart part) {
    }

    @Override
    public void createPartControl(Composite c) {
        IWorkspace workspace = ResourcesPlugin.getWorkspace();
        workspace.addResourceChangeListener(this);
        GridLayoutFactory.swtDefaults().numColumns(1).applyTo(c);
        viewer = new TreeViewer(c, SWT.SINGLE);
        GridDataFactory.swtDefaults().applyTo(viewer.getTree());
        GridDataFactory.fillDefaults().grab(true, true).applyTo(viewer.getTree());
        viewer.setContentProvider(new TreeContentProvider());
        viewer.setLabelProvider(new TreeLabelProvider());
        viewer.setInput(tree);
        viewer.addDoubleClickListener(this);
        viewer.addTreeListener(this);

        MenuManager menuMgr = new MenuManager();
        menuMgr.setRemoveAllWhenShown(true);
        menuMgr.addMenuListener(this);
        Menu menu = menuMgr.createContextMenu(viewer.getControl());
        viewer.getControl().setMenu(menu);
        getSite().registerContextMenu(menuMgr, viewer);

        final IWorkbench workbench = PlatformUI.getWorkbench();
        workbench.getActiveWorkbenchWindow().getActivePage().addPartListener(this);
    }

    @Override
    public void dispose() {
        IWorkspace workspace = ResourcesPlugin.getWorkspace();
        workspace.removeResourceChangeListener(this);
        final IWorkbench workbench = PlatformUI.getWorkbench();
        workbench.getActiveWorkbenchWindow().getActivePage().removePartListener(this);
        super.dispose();
    }

    @Override
    public void setFocus() {
    }

    @Override
    public void resourceChanged(IResourceChangeEvent event) {
        tree.refresh();
        // We may not be on the UI thread
        final IWorkbench workbench = PlatformUI.getWorkbench();
        workbench.getDisplay().asyncExec(new Runnable() {
            @Override
            public void run() {
                viewer.refresh();
            }
        });
    }

    @Override
    public void doubleClick(DoubleClickEvent event) {
        IStructuredSelection selection = (IStructuredSelection)event.getSelection();
        if (selection == null || selection.isEmpty() || !(selection.getFirstElement() instanceof IExplainableNode)) {
            return;
        }
        IExplainableNode node = (IExplainableNode)selection.getFirstElement();

        if (node instanceof FormulaNode) {
            explainFormula(node);
        } else if ((node instanceof CheckNode && !((CheckNode)node).isParameterized())
                || (node instanceof ParameterizedCheckNode))
        {
            explainDefs(node);
        }
    }

    @Override
    public void treeCollapsed(TreeExpansionEvent event) {
    }

    @Override
    public void treeExpanded(TreeExpansionEvent event) {
        if ((event.getElement() instanceof CheckNode && !((CheckNode)event.getElement()).isParameterized())
                || event.getElement() instanceof ParameterizedCheckNode)
        {
            Display.getCurrent().asyncExec(new Runnable() {
                @Override
                public void run() {
                    viewer.expandToLevel(event.getElement(), AbstractTreeViewer.ALL_LEVELS);
                }
            });
        }
    }

    private void createCounterExampleTrace(String name, VerificationResult r, MtlFormula phi, MtlFormula check,
            TraceView view)
    {
        ModifiableTrace trace = (ModifiableTrace)view.getTrace();
        MtlResult res = r.getResult(check);

        ExplanationTable ex = res.getExplanation();
        List<? extends State> states = ex.getTrace();

        lastLaneNumber++;
        if (phi instanceof StlFormula) {
            addStlExplanationClaims(name, r, (StlFormula)phi, trace);
        } else {
            addMtlExplanationEvents(name, phi, states, trace, ex);
        }

        setGrouping(view);
    }

    private void setGrouping(TraceView view) {
        Set<String> groupAtts = new HashSet<>();
        if (view.getViewConfiguration().getGroupingAttributes(TracePart.EVENT) != null) {
            groupAtts.addAll(view.getViewConfiguration().getGroupingAttributes(TracePart.EVENT));
        }
        groupAtts.add("phi");
        groupAtts.add("laneNumber");
        view.getViewConfiguration().setGroupingAttributes(TracePart.EVENT, groupAtts);

        Set<String> groupAtts2 = new HashSet<>();
        if (view.getViewConfiguration().getGroupingAttributes(TracePart.CLAIM) != null) {
            groupAtts2.addAll(view.getViewConfiguration().getGroupingAttributes(TracePart.CLAIM));
        }
        groupAtts2.add("phi");
        groupAtts2.add("laneNumber");
        view.getViewConfiguration().setGroupingAttributes(TracePart.CLAIM, groupAtts2);
        view.update();
    }

    private void addMtlExplanationEvents(String name, MtlFormula phi, List<? extends State> states,
            ModifiableTrace trace, ExplanationTable ex)
    {
        for (Region region: ex.getRegions(phi)) {
            if (region.getValue() != InformativePrefix.NOT_COMPUTED) {
                String color = colorFromSat(region.getValue());
                List<IEvent> events = new ArrayList<>();
                for (int i = region.getStartIndex(); i <= region.getEndIndex(); i++) {
                    // Add an event at every state in the region
                    Event e = new Event(states.get(i).getTimestamp());
                    e.setAttribute("phi", name);
                    e.setAttribute("color", color);
//                    e.setAttribute("type", "MTL");
                    e.setAttribute("sat", region.getValue().toString());
                    e.setAttribute("laneNumber", lastLaneNumber.toString());
                    events.add(e);
                }
                trace.addEvents(events);
            }
        }
    }

    private String colorFromSat(InformativePrefix sat) {
        switch (sat) {
            case FALSE:
                return "dark_red";
            case TRUE:
                return "dark_green";
            case NON_INFORMATIVE:
                return "dark_blue";
            case STILL_FALSE:
                return "light_red";
            case STILL_TRUE:
                return "light_green";
            case NOT_COMPUTED: // should never occur
                return "gray";
            default:
                throw new IllegalArgumentException();
        }
    }

    private void addStlExplanationClaims(String name, VerificationResult r, StlFormula phi, ModifiableTrace trace) {
        if (!isGlobally(phi) && r.contains(phi)) {
            IPsop p = phi.getSignal();
            List<IClaim> claims = PsopHelper.createClaimRepresentationOfSatisfaction(p, name);
            trace.addClaims(claims);
        }
    }

    private boolean isGlobally(StlFormula phi) {
        if (phi instanceof StlNeg) {
            StlFormula phi2 = ((StlNeg)phi).getFormula();
            if (phi2 instanceof StlUntil) {
                StlFormula f1 = ((StlUntil)phi2).getLeft();
                if (f1 instanceof StlTrue && ((StlUntil)phi2).isUntimed()) {
                    return true;
                }
            }
        }
        return false;
    }

    private final class TreeContentProvider implements ITreeContentProvider {
        @Override
        public void dispose() {
        }

        @Override
        public void inputChanged(Viewer arg0, Object arg1, Object arg2) {
        }

        @Override
        public Object[] getChildren(Object o) {
            if (o instanceof FileNode) {
                return ((FileNode)o).getSpecs().toArray();
            }
            if (o instanceof SpecNode) {
                return ((SpecNode)o).getChecks().toArray();
            }
            if (o instanceof CheckNode) {
                if (!((CheckNode)o).isParameterized()) {
                    return new Object[] {((CheckNode)o).getFormulaNode()};
                } else {
                    return ((CheckNode)o).getSubChecks().toArray();
                }
            }
            if (o instanceof ParameterizedCheckNode) {
                return new Object[] {((ParameterizedCheckNode)o).getFormulaNode()};
            }
            if (o instanceof FormulaNode) {
                return ((FormulaNode)o).getSubFormulas().toArray();
            }
            return new Object[0];
        }

        @Override
        public Object[] getElements(Object input) {
            return tree.traces.toArray();
        }

        @Override
        public Object getParent(Object o) {
            return null;
        }

        @Override
        public boolean hasChildren(Object o) {
            if (o instanceof FileNode) {
                return !((FileNode)o).getSpecs().isEmpty();
            }
            if (o instanceof SpecNode) {
                return !((SpecNode)o).getChecks().isEmpty();
            }
            if (o instanceof CheckNode) {
                return ((CheckNode)o).isParameterized() || ((CheckNode)o).getFormulaNode() != null;
            }
            if (o instanceof ParameterizedCheckNode) {
                return ((ParameterizedCheckNode)o).getFormulaNode() != null;
            }
            if (o instanceof FormulaNode) {
                return !((FormulaNode)o).getSubFormulas().isEmpty();
            }
            return false;
        }
    }

    private final class TreeLabelProvider implements ILabelProvider {
        private Image good;

        private Image bad;

        private Image neutral;

        private Image still_good;

        private Image still_bad;

        private Image formula;

        public TreeLabelProvider() {
            Bundle bundle = Platform.getBundle("org.eclipse.trace4cps.ui");
            URL fullPathString = FileLocator.find(bundle, new Path("icons/passed.png"), null);
            ImageDescriptor imageDesc = ImageDescriptor.createFromURL(fullPathString);
            good = imageDesc.createImage();

            ImageData id = good.getImageData();
            for (int i = 0; i < id.alphaData.length; i++) {
                id.alphaData[i] = (byte)(Byte.toUnsignedInt(id.alphaData[i]) / 2);
            }
            still_good = new Image(Display.getCurrent(), id);

            fullPathString = FileLocator.find(bundle, new Path("icons/delete.png"), null);
            imageDesc = ImageDescriptor.createFromURL(fullPathString);
            bad = imageDesc.createImage();

            id = bad.getImageData();
            for (int i = 0; i < id.alphaData.length; i++) {
                id.alphaData[i] = (byte)(Byte.toUnsignedInt(id.alphaData[i]) / 2);
            }
            still_bad = new Image(Display.getCurrent(), id);

            fullPathString = FileLocator.find(bundle, new Path("icons/help_contents.png"), null);
            imageDesc = ImageDescriptor.createFromURL(fullPathString);
            neutral = imageDesc.createImage();

            fullPathString = FileLocator.find(bundle, new Path("icons/formula.png"), null);
            imageDesc = ImageDescriptor.createFromURL(fullPathString);
            formula = imageDesc.createImage();
        }

        @Override
        public void addListener(ILabelProviderListener arg0) {
        }

        @Override
        public void dispose() {
            good.dispose();
            bad.dispose();
            neutral.dispose();
        }

        @Override
        public boolean isLabelProperty(Object arg0, String arg1) {
            return false;
        }

        @Override
        public void removeListener(ILabelProviderListener arg0) {
        }

        @Override
        public Image getImage(Object o) {
            if (o instanceof CheckNode && !((CheckNode)o).isParameterized()) {
                CheckNode cn = (CheckNode)o;
                if (cn.informative() == InformativePrefix.TRUE) {
                    return good;
                } else if (cn.informative() == InformativePrefix.FALSE) {
                    return bad;
                } else if (cn.informative() == InformativePrefix.STILL_TRUE) {
                    return still_good;
                } else if (cn.informative() == InformativePrefix.STILL_FALSE) {
                    return still_bad;
                } else {
                    return neutral;
                }
            } else if (o instanceof ParameterizedCheckNode) {
                ParameterizedCheckNode pn = (ParameterizedCheckNode)o;
                if (pn.informative() == InformativePrefix.TRUE) {
                    return good;
                } else if (pn.informative() == InformativePrefix.FALSE) {
                    return bad;
                } else if (pn.informative() == InformativePrefix.STILL_TRUE) {
                    return still_good;
                } else if (pn.informative() == InformativePrefix.STILL_FALSE) {
                    return still_bad;
                } else {
                    return neutral;
                }
            } else if (o instanceof FormulaNode) {
                return formula;
            } else {
                ISharedImages im = PlatformUI.getWorkbench().getSharedImages();
                return im.getImage(ISharedImages.IMG_OBJ_FILE);
            }
        }

        @Override
        public String getText(Object o) {
            if (o instanceof FileNode) {
                return ((FileNode)o).getTraceFile().getName();
            }
            if (o instanceof SpecNode) {
                return ((SpecNode)o).getLabel();
            }
            if (o instanceof CheckNode) {
                CheckNode cn = (CheckNode)o;
                if (cn.isParameterized()) {
                    return cn.getCheckName() + " [" + cn.getSubChecks().size() + "]";
                }
                return cn.getCheckName() + " – " + cn.informative().toString();
            }
            if (o instanceof ParameterizedCheckNode) {
                ParameterizedCheckNode pn = (ParameterizedCheckNode)o;
                return pn.getCheckName() + " – " + pn.informative().toString();
            }
            if (o instanceof FormulaNode) {
                return ((FormulaNode)o).getLabel();
            }
            return o.toString();
        }
    }

    @Override
    public void menuAboutToShow(IMenuManager m) {
        IStructuredSelection selection = (IStructuredSelection)viewer.getSelection();
        Object sel = selection.getFirstElement();

        ImageDescriptor arrow = PlatformUI.getWorkbench().getSharedImages()
                .getImageDescriptor(ISharedImages.IMG_TOOL_FORWARD);

        if (sel instanceof SpecNode) {
            SpecNode sn = (SpecNode)sel;
            String label = sn.getLabel();
            TraceView view = sn.getView();
            String specFile = sn.getSpecFile().getAbsolutePath();
            m.add(new Action("Re-check") {
                @Override
                public void run() {
                    view.clearExtensions(TracePart.ALL);
                    tree.removeSpec(sn);
                    try {
                        VerifyUtil.verifyChooseSemantics(view, specFile);
                    } catch (TraceException e) {
                        IStatus status = new Status(IStatus.ERROR, getClass(), e.getMessage(), e);
                        ErrorDialog.openError(null, "Error", "Failed to check " + label, status);
                    }
                    view.update();
                }
            });
        }

        if (sel instanceof FormulaNode) {
            FormulaNode fn = (FormulaNode)sel;
            Formula etl = fn.getInnerEtl();
            m.add(new Action("Explain formula — show value of this (sub)formula per checked event", arrow) {
                @Override
                public void run() {
                    explainFormula(fn);
                }
            });
            if (FormulaHelper.isTemporalOperator(etl)) {
                m.add(new Action("Explain intervals — show where temporal operator was checked") {
                    @Override
                    public void run() {
                        explainTemporalIntervals(fn);
                    }
                });
            }
        } else if ((sel instanceof CheckNode && !((CheckNode)sel).isParameterized())
                || sel instanceof ParameterizedCheckNode)
        {
            IExplainableNode node = (IExplainableNode)sel;
            m.add(new Action("Explain (sub)properties — show value of checks and defs per event", arrow) {
                @Override
                public void run() {
                    explainDefs(node);
                }
            });
            m.add(new Action("Explain all intervals — show where all temporal operators were checked") {
                @Override
                public void run() {
                    explainAllTemporalIntervals(node);
                }
            });
            m.add(new Action("Explain attributes — show value of every atomic proposition (attribute name and value)") {
                @Override
                public void run() {
                    explainAtomicPropositions(node);
                }
            });

            if (!MtlUtil.isTimed(node.getCheck())) {
                m.add(new Action("Explain LTL cause of violation (finite)") {
                    @Override
                    public void run() {
                        explainLTLCause(node, false, true);
                    }
                });
                m.add(new Action("Explain LTL cause of violation (weak)") {
                    @Override
                    public void run() {
                        explainLTLCause(node, false, false);
                    }
                });
                m.add(new Action("Explain LTL cause of violation (finite, split attributes)") {
                    @Override
                    public void run() {
                        explainLTLCause(node, true, true);
                    }
                });
                m.add(new Action("Explain LTL cause of violation (weak, split attributes)") {
                    @Override
                    public void run() {
                        explainLTLCause(node, true, false);
                    }
                });
            }

            m.add(new Action("Explain MTL cause of violation") {
                @Override
                public void run() {
                    explainMTLCause(node, false, false);
                }
            });
            m.add(new Action("Explain MTL cause of violation (split attributes)") {
                @Override
                public void run() {
                    explainMTLCause(node, true, false);
                }
            });
            m.add(new Action("Explain MTL cause of violation with colors") {
                @Override
                public void run() {
                    explainMTLCause(node, false, true);
                }
            });
            m.add(new Action("Explain MTL cause of violation with colors (split attributes)") {
                @Override
                public void run() {
                    explainMTLCause(node, true, true);
                }
            });
        }
    }

    private void explainFormula(IExplainableNode node) {
        createCounterExampleTrace(node.getExplanationLabel(), node.getResult(), node.getFormula(), node.getCheck(),
                node.getTraceView());
    }

    private void explainTemporalIntervals(FormulaNode fn) {
        Formula etl = fn.getInnerEtl();
        MtlFormula phi = fn.getFormula();
        assert FormulaHelper.isTemporalOperator(etl);
        MtlFormula until;
        if (phi instanceof MTLuntil) {
            // either a U b or F a ≡ true U a
            until = phi;
        } else if (phi instanceof MTLnot) {
            // G a ≡ ¬(true U ¬a)
            assert ((MTLnot)phi).getChild() instanceof MTLuntil;
            until = ((MTLnot)phi).getChild();
        } else {
            throw new IllegalStateException();
        }
        MtlFormula check = fn.getCheck();
        VerificationResult vr = fn.getResult();
        TabularExplanationTable table = (TabularExplanationTable)vr.getResult(check).getExplanation();

        Resource res = new Resource(100, false);
        List<IClaim> claims = new ArrayList<>();
        lastLaneNumber++;
        for (Region region: table.getIntervals(until)) {
            Claim c = new Claim(region.getStartTime(), region.getEndTime(), res, 0.1);
            c.setAttribute("phi", fn.getExplanationLabel());
            c.setAttribute("color", "gray");
            c.setAttribute("laneNumber", lastLaneNumber.toString());
            claims.add(c);
        }
        ((ModifiableTrace)fn.getTraceView().getTrace()).addClaims(claims);
        setGrouping(fn.getTraceView());
    }

    private void explainAllTemporalIntervals(IExplainableNode node) {
        VerificationResult vr = node.getResult();
        MtlFormula check = node.getCheck();
        TabularExplanationTable table = (TabularExplanationTable)vr.getResult(check).getExplanation();

        lastLaneNumber++;
        Resource res = new Resource(100, false);
        List<IClaim> claims = new ArrayList<>();
        for (MtlFormula phi: MtlUtil.getSubformulas(check)) {
            for (Region region: table.getIntervals(phi)) {
                Claim c = new Claim(region.getStartTime(), region.getEndTime(), res, 0.1);
                c.setAttribute("phi", node.getCheckName() + " intervals");
                c.setAttribute("color", "gray");
                c.setAttribute("laneNumber", lastLaneNumber.toString());
                claims.add(c);
            }
        }
        ((ModifiableTrace)node.getTraceView().getTrace()).addClaims(claims);
        setGrouping(node.getTraceView());
    }

    private void explainAtomicPropositions(IExplainableNode node) {
        VerificationResult vr = node.getResult();
        List<? extends State> trace = node.getTraceView().getTrace(0, false).getEvents();
        Collection<AttributeAtom> atoms = vr.getAtomsForCheck(node.getCheckName());
        Collection<AtomicProposition> phis = new ArrayList<>();
        for (AttributeAtom atom: atoms) {
            phis.add(new DefaultAtomicProposition(atom.getType(), atom.getKey(), atom.getVal()));
        }

        try {
            ExplanationTable l = VerifyUtil.verifyAttributes(trace, phis);
            for (AtomicProposition phi: phis) {
                lastLaneNumber++;
                addMtlExplanationEvents(phi.toString(), phi, trace, (ModifiableTrace)node.getTraceView().getTrace(), l);
            }
            setGrouping(node.getTraceView());
        } catch (TraceException e) {
            ErrorDialog.openError(null, "Error", "An error occurred while trying to add explanation",
                    new Status(IStatus.ERROR, getClass(), e.getMessage(), e));
        }
    }

    private void explainDefs(IExplainableNode node) {
        VerificationResult vr = node.getResult();
        MtlFormula check = node.getCheck();
        TabularExplanationTable table = (TabularExplanationTable)vr.getResult(check).getExplanation();

        List<MtlFormula> phis = MtlUtil.getSubformulas(check);
        for (MtlFormula phi: phis) {
            if (vr.contains(phi)) {
                lastLaneNumber++;
                String name = vr.getQuantName(phi);
                ModifiableTrace trace = (ModifiableTrace)node.getTraceView().getTrace();
                if (phi instanceof StlFormula) {
                    addStlExplanationClaims(name, vr, (StlFormula)phi, trace);
                } else {
                    addMtlExplanationEvents(name, phi, table.getTrace(), trace, table);
                }
            }
        }
        setGrouping(node.getTraceView());
    }

    private void explainLTLCause(IExplainableNode node, boolean splitAPs, boolean finite) {
        MtlFormula phi = node.getCheck();
        List<? extends State> trace = node.getTraceView().getTrace(0, false).getEvents();

        try {
            LtlNnf nnf = LtlNnf.fromMtlFormula(phi, splitAPs);
            Collection<Cause> causes = LtlCausalityChecker.computeCauses(trace, nnf, true, finite);

            lastLaneNumber++;
            List<IEvent> events = new ArrayList<>();
            for (Cause cause: causes) {
                // Add an event at every state in the region
                State s = trace.get(cause.getStateIndex());
                AtomicProposition ap = cause.getAP();
                InformativePrefix sat = s.satisfies(ap) ? InformativePrefix.TRUE : InformativePrefix.FALSE;
                Event e = new Event(s.getTimestamp());
                e.setAttribute("phi", String.format("(%s) %s", node.getCheckName(), ap));
                e.setAttribute("color", colorFromSat(sat));
                e.setAttribute("sat", sat.toString());
                e.setAttribute("laneNumber", lastLaneNumber.toString());
                events.add(e);
            }
            ((ModifiableTrace)node.getTraceView().getTrace()).addEvents(events);
            setGrouping(node.getTraceView());
        } catch (Throwable e) {
            ErrorDialog.openError(null, "Error", "An error occurred while trying to add explanation",
                    new Status(IStatus.ERROR, getClass(), e.getMessage(), e));
        }
    }

    private void explainMTLCause(IExplainableNode node, boolean splitAPs, boolean color) {
        MtlFormula phi = node.getCheck();
        List<? extends State> trace = node.getTraceView().getTrace(0, false).getEvents();

        try {
            MtlFormula nnf = MtlNnf.toNnf(phi, splitAPs);
            RecursiveMemoizationCausalityChecker rmcc = new RecursiveMemoizationCausalityChecker();
            rmcc.check(trace, nnf, ImmutableSet.of(), VerificationSemantics.Finite);
            Collection<Cause> causes = rmcc.getCauses();

            lastLaneNumber++;
            List<IEvent> events = new ArrayList<>();

            HashMap<IAttributeAware, Paint> highlightMap = new HashMap<>();

            for (Cause cause: causes) {
                // Add an event at every state in the region
                State s = trace.get(cause.getStateIndex());
                if (color) {
                    if (s instanceof ClaimEvent) {
                        IClaim claim = ((ClaimEvent)s).getClaim();
                        highlightMap.put(claim, Color.blue);
                        highlightMap.put(claim.getStartEvent(), Color.blue);
                        highlightMap.put(claim.getEndEvent(), Color.blue);
                    } else {
                        highlightMap.put(s, Color.blue);
                    }
                }
                AtomicProposition ap = cause.getAP();
                InformativePrefix sat = s.satisfies(ap) ? InformativePrefix.TRUE : InformativePrefix.FALSE;
                Event e = new Event(s.getTimestamp());
                e.setAttribute("phi", String.format("(%s) %s", node.getCheckName(), ap));
                e.setAttribute("color", colorFromSat(sat));
                e.setAttribute("sat", sat.toString());
                e.setAttribute("laneNumber", lastLaneNumber.toString());
                events.add(e);
            }
            ((ModifiableTrace)node.getTraceView().getTrace()).addEvents(events);
            node.getTraceView().getViewConfiguration().setHighlightMap(highlightMap);
            setGrouping(node.getTraceView());
        } catch (Throwable e) {
            ErrorDialog.openError(null, "Error", "An error occurred while trying to add explanation",
                    new Status(IStatus.ERROR, getClass(), e.getMessage(), e));
        }
    }
}
